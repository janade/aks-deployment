variable "name" {
  default = "zum"
}
variable "environment" {
  default = "dev"
}

variable "vnet_name" {
  type    = string
  default = "zum-aks_Vnet"
}

variable "location" {
  #default = "germanywestcentral"
  default  =  "eastasia"
}

variable "address_space" {
  type    = string
  default = "10.0.0.0/8"
}

variable "tags" {
  type        = map(string)
  description = "Any tags that should be present on the Virtual Network resources"
  default     = {}

}

