resource "azurerm_virtual_network" "vnet" {
  name                = var.vnet_name
  location            = var.location
  address_space       = [var.address_space]
  resource_group_name = "${var.name}-aks-${var.environment}-rg"
  tags                = var.tags

  lifecycle {
    ignore_changes = [
      tags
    ]
  }
}
