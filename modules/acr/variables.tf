variable "name" {}
variable "environment" {}
variable "resource_group" {}
variable "location" {}
variable "sku" {
  default = "Standard"
}

variable "tags" {
  type        = map(string)
  description = "Any tags that should be present on the Virtual Network resources"
  default     = {}
}
