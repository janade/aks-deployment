


data "azurerm_key_vault_secret" "keyvaultstoragename" {
  name         = "SAname"
  key_vault_id = var.keyvaultID
}

data "azurerm_key_vault_secret" "keyvaultstoragekey" {
  name         = "SAkey"
  key_vault_id = var.keyvaultID
}



data "azurerm_key_vault_secret" "keyvaultclientID" {
  name         = "AKSclient"
  key_vault_id = var.keyvaultID
}

data "azurerm_key_vault_secret" "keyvaultclientsecret" {
  name         = "AKSclientsecret"
  key_vault_id = var.keyvaultID
}


resource "azurerm_resource_group" "rg_aks" {
  name     = "${var.name}-aks-${var.environment}-rg"
  location = var.location
  tags     = var.tags
}

module "vnet" {
  source = "./modules/vnet"

  vnet_name  = var.vnet_name
  depends_on = [azurerm_resource_group.rg_aks]
}


resource "azurerm_subnet" "aks" {
  name                 = var.subnet_name
  resource_group_name  = azurerm_resource_group.rg_aks.name
  virtual_network_name = var.vnet_name
  address_prefixes     = [var.vnet_subnet]
  depends_on           = [module.vnet]
}

resource "azurerm_subnet" "endpoint" {
  name                 = "endpoint"
  resource_group_name  = azurerm_resource_group.rg_aks.name
  virtual_network_name = var.vnet_name
  address_prefixes     = [var.endpoint-subnet]

  enforce_private_link_endpoint_network_policies = true
  depends_on                                     = [module.vnet]
}


module "acr" {
  source = "./modules/acr"

  name           = var.name
  environment    = var.environment
  resource_group = azurerm_resource_group.rg_aks.name
  location       = azurerm_resource_group.rg_aks.location
  sku            = var.acr_sku
  depends_on     = [azurerm_resource_group.rg_aks]
}



resource "tls_private_key" "ssh" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                = "${var.name}-aks-${var.environment}"
  resource_group_name = azurerm_resource_group.rg_aks.name
  location            = azurerm_resource_group.rg_aks.location
  kubernetes_version  = var.kubernetes_version
  dns_prefix          = var.dns_prefix
  default_node_pool {
    name                = "${var.name}${var.environment}"
    node_count          = var.cnt
    vm_size             = var.vm_size
    enable_auto_scaling = true
    max_count           = 3
    min_count           = 1
    max_pods            = var.max_pods
    os_disk_size_gb     = var.os_disk_size_gb
    vnet_subnet_id      = azurerm_subnet.endpoint.id
    node_labels = {
      inodepool-type = "system"
      environment    = var.environment
      nodepoolos     = "linux"
      app            = "system-apps"
    }
  }
  linux_profile {
    admin_username = var.admin_username
    ssh_key {
      key_data = tls_private_key.ssh.public_key_openssh
    }
  }
  service_principal {
    client_id     = data.azurerm_key_vault_secret.keyvaultclientID.value
    client_secret = data.azurerm_key_vault_secret.keyvaultclientsecret.value
  }
  network_profile {
    network_plugin    = "kubenet"
    load_balancer_sku = "standard"
  }
  tags       = var.tags
  depends_on = [azurerm_resource_group.rg_aks, azurerm_subnet.endpoint]
}


