output "location_name" {
  description = "Location which all the resources will be created"
  value       = var.location
}

output "cluster_name" {
  description = "Cluster name to be used in the context of kubectl"
  value       = azurerm_kubernetes_cluster.aks.name
}

output "resource_group_name" {
  description = "Resource group name contains all the resources"
  value       = azurerm_resource_group.rg_aks
}

output "postgresql_server_name" {
  description = "Azure Postgresql database server name"
  value       = azurerm_postgresql_server.dev-database-server.name
}

output "postgresql_database_name" {
  description = "Created database name on Postgresql server"
  value       = azurerm_postgresql_database.dev-database.name
}

output "private_endpoint_name" {
  description = "Private endpoint to be used privately"
  value       = azurerm_private_endpoint.endpoint.name
}


