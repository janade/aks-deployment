data "azurerm_key_vault_secret" "keyvaultdbuser" {
  name         = "dbuseradmin"
  key_vault_id = var.keyvaultID
}

data "azurerm_key_vault_secret" "keyvaultdbpassword" {
  name         = "dbpassword"
  key_vault_id = var.keyvaultID
}


resource "azurerm_postgresql_server" "dev-database-server" {
  name                = var.postgresql_server_name
  location            = var.location
  resource_group_name = "${var.name}-aks-${var.environment}-rg"
  sku_name            = var.postgresql_sku_name

  storage_mb                   = var.database_size_mb
  backup_retention_days        = 7
  geo_redundant_backup_enabled = false
  auto_grow_enabled            = true

  administrator_login              = data.azurerm_key_vault_secret.keyvaultdbuser.value
  administrator_login_password     = data.azurerm_key_vault_secret.keyvaultdbpassword.value
  version                          = var.db_version
  ssl_enforcement_enabled          = true
  public_network_access_enabled    = false
  ssl_minimal_tls_version_enforced = "TLS1_2"

  depends_on = [azurerm_resource_group.rg_aks, module.vnet]
}

resource "azurerm_postgresql_database" "dev-database" {
  name                = var.database_name
  resource_group_name = "${var.name}-aks-${var.environment}-rg"
  server_name         = azurerm_postgresql_server.dev-database-server.name
  charset             = var.db_charset
  collation           = "English_United States.1252"
  depends_on          = [azurerm_postgresql_server.dev-database-server]
}



resource "azurerm_private_endpoint" "endpoint" {
  name                = format("%s-%s", var.name, "private-endpoint")
  location            = var.location
  resource_group_name = "${var.name}-aks-${var.environment}-rg"
  subnet_id           = azurerm_subnet.endpoint.id

  private_service_connection {
    name                           = format("%s-%s", var.name, "privateserviceconnection")
    private_connection_resource_id = azurerm_postgresql_server.dev-database-server.id
    is_manual_connection           = false
    subresource_names              = ["postgresqlServer"]
  }
}



