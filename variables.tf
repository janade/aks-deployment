variable "name" {}
variable "environment" {}
variable "location" {}


variable "dns_prefix" {
  default = "k8dev"
}

variable "keyvaultID" {
  type        = string
  default     = "ECB-AKS-keyvault"
  description = "keyvaultclientID"
}

variable "vnet_name" {
  type    = string
  default = "ZUM-aks_Vnet"
}

variable "subnet_name" {
  type    = string
  default = "ZUM-aks-subnet"
}

variable "vnet_subnet" {
  type    = string
  default = "10.1.0.0/24"
}

variable "endpoint-subnet" {
  type    = string
  default = "10.224.0.0/16"
}


variable "address_space" {
  type    = string
  default = "10.0.0.0/8"
}

variable "acr_sku" {
  default = "Standard"
}


variable "kubernetes_version" {
  default = "1.23.8"
}

variable "cnt" {
  default = "1"
}
variable "vm_size" {
  default = "standard_e2bds_v5"
}

variable "max_pods" {
  default = "30"
}

variable "os_type" {
  default = "Linux"
}

variable "os_disk_size_gb" {
  default = "30"
}

variable "admin_username" {
  default = "ubuntu"
}


variable "tags" {
  type        = map(string)
  description = "Any tags that should be present on the Virtual Network resources"
  default     = {}
}


variable "postgresql_server_name" {
  default = "zum-dev-database-server"
}

variable "postgresql_sku_name" {
  default = "GP_Gen5_2"
}

variable "database_size_mb" {
  type    = number
  default = 10240
}

variable "database_name" {
  default = "devdatabase"
}
variable "db_charset" {
  default = "UTF8"
}
variable "db_version" {
  default = "11"
}



