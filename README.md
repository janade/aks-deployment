**READ ME: Deployment of AKS with a Database **

1. Abstract
2. Prerequisites
3. Requirements
4. Things to consider
5. Terraform scripts and Variables
6. Implementation

**#Abstract:**

The main objective is to create a kubernetes cluster on azure cloud with its dependencies including; _Resource Group, Virtual Network, Subnets, Account Storage_ and _Key vault_. The prerequisites will be addressed and highlighted along with deployment requirements, things to consider as well as defining variables.

In order to create AKS Terraform scripts, the necessary resources required need to be created in AKS and PostgreSQL databases.

- **Resource group:** We will need to create a Resource Group and then create a Key Vault and Account Storage inside it.

- **Key Vault:** Create a Key Vault in which to save the credentials of the PostgreSQL database, the service principal credentials as well as the Account Storage Secret.

- **Account Storage:** create an Account Storage with the file shared. This is required to store the Terraform status file

In the repository we have _prereq.azcli_ script, which will create the target Account Storage with the previously mentioned file shared. This is inside the created Resource Group.

- **Service Principal:** is to authenticate the proper azure cloud account with terraform scripts.

**#Requirements:**

One can use CloudShell to implement and test the Terraform scripts. Alternatively one can create a Virtual Machine and prepare it by installing the proper packages needed to initiate, test, and apply the Terraform scripts on Azure Cloud after authenticating the VM with the proper azure account. .

**#Things to be considered:**

Make sure that a Service Principal should have the proper permissions to handle all the creations of  resources inside the scripts.

Service Principal credentials should be saved in the created Key Vault and configure them in Terraform script **main.tf **

Additionally the created Service Principal should have the appropriate permission to access the created Key Vault to retrieve the values/credentials from it.

Make sure to enter the created Account storage name and secret in a Key Vault, then configure the Account Storage inside the Terraform script **terraform.tf**

PostgreSQL database credentials should be saved in the created Key Vault and configured in terraform script Postgresql.tf  as well as in variables.

Change the values in all Terraform variables.tf files as desirable as well as for Terraform **terraform.tfvars** variable file.

**#Terraform scripts and variables**

There are multiple Terraform scripts, the following will explain each script separately:

**terraform.tf:** is a file to define the minimum required Terraform version with the backend Account Storage details that is necessary for saving the Terraform status.

**main.tf** : is the main Terraform script. In this script we will apply the following operations:

- Define the Provider (we will choose Azure since we need to implement it in Azure Cloud).
- Let Terraform save the status on the created Account Storage by retrieving the Account Storage credentials from the created Key Vault.
- Retrieve the Credentials of Service Principal from the created Key Vault, those would be used later with AKS creation.
- Create a Resource Group.
- Create a Virtual Network inside the newly created Resource Group, in this step we will call a module from the source directory module/vnet.
- Create two Subnets, one for AKS and another Subnet for Endpoint within the Virtual Network created in the previous step.
- Create an Azure Container Registry (ACR) inside the Resource Group. In this step we will call another module from the source directory module/acr.
- Create a SSH private key using the RSA algorithm with 4096 bits for AKS.
- Create AKS inside the Resource Group with default NodePool with a maximum Node count 3 using Linux as OS. The used Container Network Interface (CNI) will be kubenet with a Standard LoadBalancer.

**terraform.tfvars:** is an input variable that allows you to share modules across different Terraform configurations, making modules composable and reusable. In this file all shared variables are declared such as Region, Resource Group name and keyvaultID.

**Postgresql.tf:** this Terraform script is to create a PostgreSQL server with a database as following:

- Retrieve the PostgreSQL credentials from the created Key Vault.
- Create PostgreSQL server inside the defined Resource Group with specification defined in variables.tf and Backup retention 7 days. The creation of the PostgreSQL server will depend on the initial creation of a defined Resource Group and a Virtual Network.
- Create the PostgreSQL database inside the defined Resource Group with the specifications defined in variable files. For the creation of the PostgreSQL database, the  PostgreSQL server will first need to be created.
- Create a Private Endpoint with Private Service Connection. This is done in order to let the PODs (created inside AKS) connect to the mentioned database privately through a private IP address.

**variables.tf:** is used to define variables for Terraform configurations such as AKS name, VM\_size for AKS, Resource Group or Region. Below are the list of used variables for the main module:

- **name** : used in a combination with other variables to form a value like Resource Group name.  It does not have any attributes because the value will be taken from global variables in terraform.tfvars.
- **environment** : one can use this variable for the used Environment. This variable is the same as the name variable. It does not have any attributes because the value will be taken from global variables in terraform.tfvars.
- **location:** this variable is used to store location variables, the value will be taken from terraform.tfvar file.
- **dns\_prefix:** this variable is used as its name implies for AKS dns\_prefix.
- **vnet\_name:** Virtual Network name.
- **subnet\_name:** AKS Subnet name.
- **vnet\_subnet:**  address\_prefixes for AKS Subnet.
- **endpoint-subnet:** address\_prefixes for Endpoint Subnet.
- **address\_space:** address\_space for the Virtual Network.
- **acr\_sku:** Azure Container Registry sku.
- **kubernetes\_version** : the used Kubernetes version.
- **cnt:** Node count in AKS.
- **vm\_size:** the VM size for the Node.
- **max\_pods:** maximum number of running PODs.
- **os\_type:** the OS used.
- **os\_disk\_size\_gb:** the disk size of AKS.
- **admin\_username:** the username for AKS.
- **tags:** generated tags.
- **postgresql\_server\_name:** PostgreSQL server name.
- **postgresql\_sku\_name:** used sku for postgreSQL server.
- **database\_size\_mb:** the size of the used database disk.
- **database\_name:** the database name.
- **db\_charset:** the used Character set for the target database.
- **db\_version:** database version.

There is a variable file for the modules acr and vnet. The same strategy is used for output.tf.

**output.tf:** Customize the Terraform output files as needed.

**#Implementation**

After setting up everything, the following steps will need to be taken to implement the script in order to create the target AKS with PostgreSQL database:

1. Initialize Terraform and install all needed dependencies:

    terraform init

2. Check if everything is ok and fixing the errors if any

    terraform plan

3. Create the target AKS with its related resources

    terraform apply
